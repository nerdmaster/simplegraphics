# -*- coding: utf-8 -*-
"""
File: simpleGraphics.py
Created on Mon Oct 27 16:40:42 2014
+------------------------------------------------------+
|(c) 2014 The University of Texas at Austin            |
|         Mechanical Enigneering Department            |
|         NERDLab - Neuro-Engineering, Research &      |
|                   Development Laboratory             |
|         @author: benito                              |
+------------------------------------------------------+
URL: http://www.maprantala.com/2010/05/16/measuring-distance-from-a-point-to-a-line-segment/
Description: Define Point, Segment and some methods
--------------------------------------------------------
Inputs: None
--------------------------------------------------------
Outputs: None
--------------------------------------------------------
"""
# Modified from: graphics.py
"""Simple object oriented graphics library

The library is designed to make it very easy for novice programmers to
experiment with computer graphics in an object oriented fashion. It is
written by John Zelle for use with the book "Python Programming: An
Introduction to Computer Science" (Franklin, Beedle & Associates).

LICENSE: This is open-source software released under the terms of the
GPL (http://www.gnu.org/licenses/gpl.html).

PLATFORMS: The package is a wrapper around Tkinter and should run on
any platform where Tkinter is available.

INSTALLATION: Put this file somewhere where Python can see it.

OVERVIEW: There are two kinds of objects in the library. The GraphWin
class implements a window where drawing can be done and various
GraphicsObjects are provided that can be drawn into a GraphWin. As a
simple example, here is a complete program to draw a circle of radius
10 centered in a 100x100 window:

--------------------------------------------------------------------
from graphics import *

def main():
    win = GraphWin("My Circle", 100, 100)
    c = Circle(Point(50,50), 10)
    c.draw(win)
    win.getMouse()#<----------------------------(pause) # Pause to view result
    win.close()    # Close window when done

main()
--------------------------------------------------------------------
GraphWin objects support coordinate transformation through the
setCoords method and pointer-based input through getMouse.

The library provides the following graphical objects:
    Point
    Line
    Circle
    Oval
    Rectangle
    Polygon
    Text
    Entry (for text-based input)
    Image

Various attributes of graphical objects can be set such as
outline-color, fill-color and line-width. Graphical objects also
support moving and hiding for animation effects.

The library also provides a very simple class for pixel-based image
manipulation, Pixmap. A pixmap can be loaded from a file and displayed
using an Image object. Both getPixel and setPixel methods are provided
for manipulating the image.

DOCUMENTATION: For complete documentation, see Chapter 4 of "Python
Programming: An Introduction to Computer Science" by John Zelle,
published by Franklin, Beedle & Associates.  Also see
http://mcsp.wartburg.edu/zelle/python for a quick reference"""
'''
Added Button(Rectangle, Text)
      Segment(Line)
      Slider
      PushButton(Button)

'''
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import sys
import distutils.version
from pyBondGraph_requirements import satisfy_requirements as test_reqs

__version__  = '0.0.1'
__version__numpy__ = '1.9.0'        # minimum required numpy version
__version__sympy__ = '0.7.5'        # minimum required sympy version
__version__scipy__ = '0.14.0'       # minimum required scipy
__version__matplotlib__ = u'1.4.0'  # minimum required matplotlib
__version__PyBondGraph__ = '0.0.1'  # minimum required PyBondGraph

#----------------------------
test_reqs()
#----------------------------

try:
    import simpleGraphics # >= 0.0.1'
    if simpleGraphics.__version__ < '0.0.1':
        ImportError("simpleGraphics requires simpleGraphics >= '0.0.1'")
except ImportError:
    raise ImportError("simpleGraphics requires simpleGraphics")

try:
    import numpy
except ImportError:
    raise ImportError("pyBondGraph requires numpy")

try:
    import sympy
except ImportError:
    raise ImportError("pyBondGraph requires sympy")

try:
    import scipy
except ImportError:
    raise ImportError("pyBondGraph requires scipy")

try:
    import matplotlib
except ImportError:
    raise ImportError("pyBondGraph requires matplotlib")

__version__numpy__ = numpy.__version__
__version__sympy__ = sympy.__version__
__version__scipy__ = scipy.__version__
__version__matplotlib__ = matplotlib.__version__
__version__PyBondGraph__ = PyBondGraph.__version__
